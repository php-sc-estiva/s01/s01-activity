<?php 

// Single Line comments [ctrl + /]
/*
	Multiline comments [ctrl + shft + /]
*/


// Variables
// Variables in PHP are defined using dollar ($) notation before the name of the variable. ex: $varName;

$name = 'John Smith';
$email = 'johnsmith@mail.com';

// Constants
// Constants in PHP are defined using the define() function

define('PI', 3.1416);

// Data types

// Strings
$state = 'New Yourk';
$country = 'United States of America';

// Integers

$age = 31;
$headcount = 27;

// Floating points
$grade = 98.2;
$distanceInKilometers = 526.28;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Null
$spouse = null;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

// Objects

$gradesObj = (object)[
		'firstGrading' => 98.7,
		'secondGrading' => 92.1,
		'thirdGrading' => 90.2,
		'fourthGrading' => 94.6
	];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'USA'
	]
];

// Operators
// Assignment operators are used to assign values to variables (=)

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// Function
function getFullName($firstName, $middleInitial, $lastName) {
	return "$lastName, $firstName $middleInitial";
}

// Selection Control Structures

// If-Elseif-else statement

function determineTyphoonIntentsity($windSpeed) {
	if($windSpeed < 30) {
		return 'Not a typhoon yet';
	}
	else if($windSpeed <= 61) {
		return 'Tropical Depression detected';
	}
	else if($windSpeed >= 62 && $windSpeed <=88) {
		return 'Tropical storm detected';
	}
	else if($windSpeed >= 89 && $windSpeed <=117) {
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected.';
	}
	
}

// Switch Statement

function determineComputerUser($computerNumber){
	switch ($computerNumber){
		case 1:
			return 'Linus Trovalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Onel De Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber . ' is out of bounds.';
	}
}

// Ternary operator
function isUnderAge($age){
	return ($age < 18) ? true : false;
}

// Try-Catch-Finally
function greeting($str){
	try {
		// Attempt to execute a code
		if(gettype($str) == "string"){
			echos $str;
		}
		else{
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}
	finally{
		// Continue execution of code regardless of success or failure of code execution in 'try' block.
		echo " I did it again!";
	}
}
